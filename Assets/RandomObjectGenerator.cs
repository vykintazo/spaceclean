﻿using UnityEngine;

public class RandomObjectGenerator : MonoBehaviour
{
    public GameObject[] obj;
    public float rangeX = 100;
    public float rangeY = 100;
    public float rangeZ = 100;
    public int objects = 100;

    
    void Start()
    {
        for(int i = 0; i < objects; i++)
        {
            Vector3 instPos = new Vector3(Random.Range(-rangeX, rangeX), Random.Range(-rangeY, rangeY), Random.Range(-rangeZ, rangeZ));
            Quaternion instRot = Quaternion.EulerAngles(new Vector3(Random.Range(0, 180), Random.Range(0, 180), Random.Range(0, 180)));
            Instantiate(obj[Random.Range(0,obj.Length)], instPos, instRot); 
        }
    }
    
}