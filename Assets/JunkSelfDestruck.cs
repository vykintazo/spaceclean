﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JunkSelfDestruck : MonoBehaviour
{
    public float zLimit= 200;
    public float explosionZ = 400f;
    public GameObject explosion;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.z > zLimit)
            GetComponent<ParticleSystem>().enableEmission = true;
        if(transform.position.z > explosionZ)
        {
            ScoreManager script = GameObject.Find("Canvas").GetComponent<ScoreManager>();
            script.score++;
            script.plus = true;
            Instantiate(explosion, transform.position,Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
