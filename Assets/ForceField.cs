﻿using UnityEngine;

public class ForceField : MonoBehaviour
{
    public string forceTogglekey = "space";
    public float pushForce = 1000;
    private ParticleSystem particleSystem;
    private bool forceField;
    public float cooldown =2;
    private float timeNow; 
    void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if(Input.GetKeyDown(forceTogglekey) && timeNow > cooldown)
        {
            particleSystem.Play();
            timeNow = 0;
        }
        timeNow += Time.deltaTime;
        if (particleSystem.isPlaying)
            forceField = true;
        else
            forceField = false;
    }
    private void OnTriggerStay(Collider other)
    {
        if(forceField)
        {
            if(other.gameObject.GetComponent<Rigidbody>() != null)
            {

                Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
                rb.AddForce(transform.up * -pushForce);
            }
        }
    }
}
