﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ScoreManager : MonoBehaviour
{
    public int score;
    public bool plus = false;
    public Animator medalText;
    public Text scoreText;
    public bool restart = false;
    float t;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score: " + score.ToString();
        if(plus)
        {
            
            medalText.SetTrigger("Play");
            plus = false;
        }
        if(restart)
        {
            t += Time.deltaTime;
            if (t > 3)
                SceneManager.LoadScene(0);
        }
    }
}
