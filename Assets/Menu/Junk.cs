﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;

public class Junk : MonoBehaviour
{
    public GameObject debris;
    // Start is called before the first frame update
    void Start()
    {
        TextAsset txt = (TextAsset)Resources.Load("tle", typeof(TextAsset));
        List<String> lines = new List<String>(txt.text.Split('\n'));
        Debug.Log(lines[0]);
        foreach(String line in lines){
            List<String> l = new List<String>(line.Split(','));
            Debug.Log(line);
            GameObject junk = Instantiate(debris,new Vector3(float.Parse(l[1], CultureInfo.InvariantCulture.NumberFormat), float.Parse(l[2], CultureInfo.InvariantCulture.NumberFormat), float.Parse(l[3], CultureInfo.InvariantCulture.NumberFormat)),transform.rotation);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

