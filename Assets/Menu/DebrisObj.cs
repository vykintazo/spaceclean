﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisObj
{
    public int ID;
    public double X;
    public double Y;
    public double Z;
}
