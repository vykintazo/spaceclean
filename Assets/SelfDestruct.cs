﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    public GameObject explosion;
    public ScoreManager manager;

    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {
            Instantiate(explosion,transform.position,transform.rotation);
            manager.restart = true;
            Destroy(gameObject);
        }
    }
}
