﻿using UnityEngine;

public class SpaceCraftControl : MonoBehaviour
{
    private Rigidbody rb;
    public float basicMovementForce;
    public float angularRotSpeed = 50;

    private float forwardVelocity;
    private float horizonatlVelocity;
    private float verticalVelocity;

    [Range(-1, 1)]
    public float angularRot;
    


    
    void Start()
    {
        rb = GetComponent<Rigidbody>();    
    }

    void Update()
    {
        Vector3 rot = new Vector3(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"),Input.GetAxis("Diagonal"));
        forwardVelocity = Input.GetAxis("Forward");
        horizonatlVelocity = Input.GetAxis("Left");
        verticalVelocity = Input.GetAxis("Up");
        transform.Rotate(rot * angularRotSpeed * Time.deltaTime);
    }
    private void FixedUpdate()
    {
        rb.AddForce(transform.forward * forwardVelocity * basicMovementForce * Time.deltaTime);
        rb.AddForce(transform.right * horizonatlVelocity * basicMovementForce * Time.deltaTime);
        rb.AddForce(transform.up * verticalVelocity * basicMovementForce * Time.deltaTime);
    }
}
